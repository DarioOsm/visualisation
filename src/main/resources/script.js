var run = true;
var point = false;

var ELASTICSEARCH_INDEX_ENDPOINT = 'http://192.168.0.104:9200/test-master';
jQuery.support.cors = true;

var METERS_PER_MILE = 1609.34;

var miles_max = 200;
var miles_step = 20;
var miles_radius = 3 * miles_step;

var max_results = 10;

var pinNumber = 1;
var colorDrivers = [];

var directionsService = new google.maps.DirectionsService();

var map, centerPt, ctrMarker, searchCircle, radiusSlider;

var circles = [];
var colors = [ 'aqua', 'black', 'blue', 'fuchsia', 'gray', 'green', 'lime',
		'maroon', 'navy', 'olive', 'orange', 'purple', 'red', 'silver', 'teal',
		'white', 'yellow' ];

var latLonLines = [];

var beginResult = 0;

function stop() {

	if (run) {
		run = false;
		$('#stopBtn').html("Start");
	} else {

		run = true;
		$('#stopBtn').html("Start");
	}

}

function drawPolygone(coords) {

	var poly = new google.maps.Polygon({
		paths : coords,
		strokeColor : '#FF0000',
		strokeOpacity : 0.8,
		strokeWeight : 2,
		fillColor : '#FF0000',
		fillOpacity : 0.35
	});

	poly.setMap(map);

}

function drawLinestring(coords) {
	var poly = new google.maps.Polyline({
		path : coords,
		geodesic : true,
		strokeColor : '#FF0000',
		strokeOpacity : 1.0,
		strokeWeight : 2

	});

	poly.setMap(map);

}

function drawPoint(lat, lon) {

	var marker = new google.maps.Marker({
		label : "1",
		cursor : "Place 1",
		map : map,
		draggable : false,
		position : new google.maps.LatLng(lat, lon),
	});
	marker.setMap(map);

}

function drawVehicleID(id, lat, lon) {
	var marker = new google.maps.Marker({
		label : "" + id,
		cursor : "Place " + id,
		map : map,
		draggable : false,
		position : new google.maps.LatLng(lat, lon),
	});
	marker.setMap(map);

}

function drawVehiclePoint(id, lat, lon) {
	var circle = new google.maps.Circle({
		strokeColor : colors[id % 16],
		strokeWeight : 1,
		fillColor : colors[id % 16],
		fillOpacity : 1,
		map : map,
		center : new google.maps.LatLng(lat, lon),
		radius : 4
	});

}

(function() {

	google.maps.event.addDomListener(window, 'load', initialize);

	function initialize() {

		centerPt = new google.maps.LatLng(57.046707, 9.935932);

		var mapOptions = {
			center : centerPt,
			zoom : 12,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById("mapcanvas"),
				mapOptions);

		setInterval(function() {
			queryAnotherBatch();
		}, 10000);

	}

	function getResults() {

		$('#results').html('');

		hash = {};
		hash.url = ELASTICSEARCH_INDEX_ENDPOINT + '/_search';
		hash.type = 'POST';
		hash.dataType = 'json';
		hash.crossDomain = true;
		hash.success = processResults;
		hash.error = function(arg) {
			console.error('ajax error: ', arg)
		};

		hash.data = JSON.stringify({
			"from" : beginResult,
			"size" : max_results,
			"query" : {
				"match_all" : {}
			}
		});

		jQuery.ajax(hash);
	}

	function processResults(json) {
		var counter = 0;
		json['hits']['hits'].map(function(i) {
			
			counter++;
			var src = i['_source'];
			src.id = i["_id"];
			var splitId = src.id.split('_');
			var driverId = splitId[0];

			src.id = driverId + '_' + splitId[1];

			if (driverId == 5 || driverId == 6  || driverId == 8 || driverId == 9) {
			var arrayLatLon = src.location.split(',');
			var lat = arrayLatLon[0];
			var lon = arrayLatLon[1];
			lat = Math.round(lat * 1000000) / 1000000;
			lon = Math.round(lon * 1000000) / 1000000;

			var utm = "+proj=utm +zone=32";
			var wgs84 = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";

			var lonLat = proj4(utm, wgs84, [ lat, lon ]);

			var position = new google.maps.LatLng(lonLat[1], lonLat[0]);

			src.formatted_lat = Math.round(lat * 1000000) / 1000000;
			src.formatted_lon = Math.round(lon * 1000000) / 1000000;
			src.formatted_id = driverId;
			if (point != true) {
				if (latLonLines[driverId] !== undefined) {

					if (latLonLines[driverId].end == null) {
						latLonLines[driverId].end = position;
					} else {
						var newStart = latLonLines[driverId].end;
						latLonLines[driverId].start = newStart;
						latLonLines[driverId].end = position;

					}

					var request = {
						origin : latLonLines[driverId].start,
						destination : latLonLines[driverId].end,
						travelMode : google.maps.TravelMode.WALKING
					};
					var driverRoute = new google.maps.DirectionsRenderer;

					driverRoute.setMap(map);
					driverRoute.setOptions({
						suppressMarkers : true,
						preserveViewport : true,
						polylineOptions: {
					      strokeColor: colors[driverId % 16]
					    }
					});

					directionsService.route(request,
							function(response, status) {
								console.log(status);

								if (status == google.maps.DirectionsStatus.OK) {

									var circle = new google.maps.Circle({
										strokeColor : colors[driverId % 16],
										strokeWeight : 1,
										fillColor : colors[driverId % 16],
										fillOpacity : 1,
										map : map,
										center : position,
										radius : 1
									});
									circles.push(circle);
					
									driverRoute.setDirections(response);
								} 
							});

				} else {
					var vozac = {
						start : position,
						end : null
					};
					latLonLines[driverId] = vozac;
				}

			} else {
				drawVehiclePoint(driverId, lonLat[1], lonLat[0]);
			}
			}
			setTimeout(console.log("AAA"), 1000);
		})
		beginResult += counter;
	}

	function queryAnotherBatch() {
		if (run) {
			getResults();
		}
	}

}).call(this);
