package visualisation

import scala.collection.mutable.Buffer
import scala.collection.mutable.ArrayBuffer
import com.vividsolutions.jts.geom.Coordinate

class Constant(
    val windowType1: Int = -1, 
    val windowValue1: Array[Int] = null, 
    val keyword1: String = null, 
    val operator1: String = null,
    val direction1: String = null,
    val valueOperator1: Double = -1.0,
    val geometryType1: String = null,
    val condition1: Boolean = false,
    val x1: Buffer[Double] = new ArrayBuffer[Double],
    val y1: Buffer[Double] =  new ArrayBuffer[Double],
    val coordinates1: Buffer[Coordinate] = new ArrayBuffer[Coordinate],
    val trajectoryType1: Int = -1) {
  
  private var _windowType: Int = windowType1
  def windowType = _windowType
  def windowType_= (value: Int):Unit = _windowType = value
  
  private var _windowValue: Array[Int] = windowValue1
  def windowValue = _windowValue
  def windowValue_= (value: Array[Int]):Unit = _windowValue = value
  
  private var _keyword: String = keyword1
  def keyword = _keyword
  def keyword_= (value: String):Unit = _keyword = value
  
  private var _operator: String = operator1
  def operator = _operator
  def operator_= (value: String):Unit = _operator = value
  
  private var _direction: String = operator1
  def direction = _direction
  def direction_= (value: String):Unit = _direction = value
  
  private var _valueOperator: Double = valueOperator1
  def valueOperator = _valueOperator
  def valueOperator_= (value: Double):Unit = _valueOperator = value
  
  private var _geometryType: String = geometryType1
  def geometryType = _geometryType
  def geometryType_= (value: String):Unit = _geometryType = value
  
  private var _condition: Boolean = condition1
  def condition = _condition
  def condition_= (value: Boolean):Unit = _condition = value
  
  private var _x: Buffer[Double] = x1
  def x = _x
  def x_= (value: Buffer[Double]):Unit = _x = value
  
  private var _y: Buffer[Double] = y1
  def y = _y
  def y_= (value: Buffer[Double]):Unit = _y = value
  
  private var _coordinates: Buffer[Coordinate] = coordinates1
  def coordinates = _coordinates
  def coordinates_= (value: Buffer[Coordinate]):Unit = _coordinates = value
  
  private var _trajectoryType: Int = trajectoryType1
  def trajectoryType = _trajectoryType
  def trajectoryType_= (value: Int):Unit = _trajectoryType = value
  
	override def toString = {
		"windowType : " + windowType + "\n" + "keyword : " + keyword + "\n" + "operator : " + operator + "\n"+ "Value operator : " + valueOperator + "\n"+ "geometryType : " + geometryType + "\n"+ "trajectoryType : " + trajectoryType + "\n"
	}
}