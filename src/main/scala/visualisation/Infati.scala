package visualisation

import java.sql.Timestamp
import mobydick._
import com.vividsolutions.jts.io.WKTReader
import com.vividsolutions.jts.geom.Geometry


case class Infati(id: Int, position: Geometry, timestamp: Timestamp) {
  def this(line: String) {
    this(line.split("#")(0).toInt, new WKTReader().read((line.split("#")(2).toString).substring(1, line.split("#")(2).toString.length() - 1)), Timestamp.valueOf(line.split("#")(4)))
  }
}

object Infati {
	def apply(line: String): Infati = {	
		var id = line.split("#")(0).toInt
		var point = new WKTReader().read((line.split("#")(2).toString).substring(1, line.split("#")(2).toString.length() - 1))
		var time = Timestamp.valueOf(line.split("#")(4).toString.substring(0, line.split("#")(4).toString.length() - 5))
		var infati = new Infati(id, point, time)
		infati
	}
}