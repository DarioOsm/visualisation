package visualisation

import mobydick.TemporalPoint

case class Mobydick(id: Int, location: TemporalPoint)
