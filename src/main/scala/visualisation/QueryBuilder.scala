package visualisation

import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import java.util.concurrent.TimeUnit
import com.vividsolutions.jts.geom._
import scala.collection.mutable.Buffer
import java.sql.Timestamp
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector
import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.streaming.api.TimeCharacteristic
import mobydick._
import visualisation.ElasticsearchUpsertSink
import ucar.unidata.geoloc.projection.UtmCoordinateConversion
import scala.util.control.Breaks
import scala.collection.mutable.Buffer
import scala.collection.mutable.ArrayBuffer
import visualisation.Mobydick

object QueryBuilder {

  val factory = new GeometryFactory()
  val writeToElasticsearch = true // set to true to write results to Elasticsearch
  val elasticsearchHost = "192.168.0.104" // look-up hostname in Elasticsearch log output
  val elasticsearchPort = 9300

  def create(params: Constant): Unit = {
    
    //Initialise streaming environment
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    val stream = env.socketTextStream("localhost", 9999)
    val geoLifeStream: DataStream[Infati] = stream.map { tuple => Infati(tuple) }

    // static objects
    var polygonOfInterest: Polygon = null;
    var linestringOfInterest: LineString = null
    var pointOfInterest: Point = null

    if (params.geometryType != null) {
      if ("Polygon".equals(params.geometryType)) {
        polygonOfInterest = createPolygon(params.coordinates)
      } else if ("Linestring".equals(params.geometryType)) {
        linestringOfInterest = createLinestring(params.coordinates)
      } else if ("Point".equals(params.geometryType)) {
        pointOfInterest = createPoint(params.coordinates)
      }
    }

    // defining if the query is static
    var staticQuery = false
    if (params.keyword.equals("WITHIN")
      || params.keyword.equals("DISJOINT")) {
      staticQuery = true
    }

    // window

    var windowExists: Boolean = params.windowType != 0
 
    var query = geoLifeStream
      .assignAscendingTimestamps(geolife => geolife.timestamp.getTime)
    var nextQuery: DataStream[(com.vividsolutions.jts.geom.Geometry, Long, Int, Double, Double)] = null

    if (!staticQuery) {
      var mobyDickStream: DataStream[Mobydick] = null
      if (windowExists) {
        var windowStream: WindowedStream[Infati, Tuple, TimeWindow] = null

        if (params.windowType == 2) {
          windowStream = query.keyBy(0)
            .timeWindow(Time.of(params.windowValue(0), TimeUnit.SECONDS), Time.of(params.windowValue(1), TimeUnit.SECONDS))
        } else if (params.windowType == 1) {
          windowStream = query.keyBy(0)
            .timeWindow(Time.of(params.windowValue(0), TimeUnit.SECONDS))
        }
        mobyDickStream = windowStream.apply { MobyDick.mobilePoint _ }
      } else {
        mobyDickStream =
          query.keyBy(0)
            .timeWindow(Time.of(1, TimeUnit.SECONDS))
            .apply { MobyDick.mobilePoint _ }
      }

      if (params.keyword.equals("TRAJECTORY LENGTH")) {
        val operator: String = params.operator
        val operatorValue: Double = params.valueOperator
        if (operator.equals(">")) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.lengthAtTime(mo.location.endTime) > operatorValue)
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        } else if (operator.equals(">=")) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.lengthAtTime(mo.location.endTime) >= operatorValue)
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        } else if (operator.equals("=")) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.lengthAtTime(mo.location.endTime) == operatorValue)
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        } else if (operator.equals("<=")) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.lengthAtTime(mo.location.endTime) <= operatorValue)
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        } else if (operator.equals("<")) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.lengthAtTime(mo.location.endTime) < operatorValue)
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        }

      } else if (params.keyword.equals("DISTANCE")) {
        val operator: String = params.operator
        val operatorValue: Double = params.valueOperator

        if (operator.equals(">")) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.distance(pointOfInterest, mo.location.endTime).asInstanceOf[Double] > operatorValue)
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        } else if (operator.equals(">=")) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.distance(pointOfInterest, mo.location.endTime).asInstanceOf[Double] >= operatorValue)
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        } else if (operator.equals("=")) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.distance(pointOfInterest, mo.location.endTime).asInstanceOf[Double] == operatorValue)
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        } else if (operator.equals("<=")) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.distance(pointOfInterest, mo.location.endTime).asInstanceOf[Double] <= operatorValue)
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        } else if (operator.equals("<")) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.distance(pointOfInterest, mo.location.endTime).asInstanceOf[Double] < operatorValue)
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        }

      } else if (params.keyword.equals("MIN DISTANCE")) {
        if ("Polygon".equals(params.geometryType)) {

          nextQuery = mobyDickStream
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, mo.location.minDistance(polygonOfInterest), -1.0))

        } else if ("Linestring".equals(params.geometryType)) {
          nextQuery = mobyDickStream
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, mo.location.minDistance(linestringOfInterest), -1.0))

        } else if ("Point".equals(params.geometryType)) {
          nextQuery = mobyDickStream
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, mo.location.minDistance(pointOfInterest), -1.0))

        }
      } else if (params.keyword.equals("MAX DISTANCE")) {
        if ("Polygon".equals(params.geometryType)) {
          nextQuery = mobyDickStream
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, mo.location.maxDistance(polygonOfInterest)))

        } else if ("Linestring".equals(params.geometryType)) {
          nextQuery = mobyDickStream
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, mo.location.maxDistance(linestringOfInterest)))
        } else if ("Point".equals(params.geometryType)) {
          nextQuery = mobyDickStream
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, mo.location.maxDistance(pointOfInterest)))

        }
      } else if (params.keyword.equals("PASSES")) {
        if ("Polygon".equals(params.geometryType)) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.passes(polygonOfInterest))
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        } else if ("Linestring".equals(params.geometryType)) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.passes(linestringOfInterest))
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        } else if ("Point".equals(params.geometryType)) {
          nextQuery = mobyDickStream
            .filter(mo => mo.location.passes(pointOfInterest))
            .map(mo => (mo.location.atFinal().geom, System.currentTimeMillis / 1000, mo.id, -1.0, -1.0))
        }
      }

    } else {
      if (params.keyword.equals("WITHIN")) {
        nextQuery = query
          .filter(geolife => geolife.position.within(polygonOfInterest))
          .map(geolife => (geolife.position, System.currentTimeMillis / 1000, geolife.id, -1.0, -1.0)) 
      } else if (params.keyword.equals("DISJOINT")) {
        nextQuery = query
          .filter(geolife => geolife.position.disjoint(polygonOfInterest))
          .map(geolife => (geolife.position, System.currentTimeMillis / 1000, geolife.id, -1.0, -1.0))
      }

    }

    nextQuery.print
    writeToElasticsearch(nextQuery)
    env.execute("Apache Flink Geospatial")

  }

  def createPolygon(coords: Buffer[Coordinate]): Polygon = {
    var ring: LinearRing = factory.createLinearRing(coords.toArray)
    var holes: Array[LinearRing] = null
    var poly: Polygon = factory.createPolygon(ring, holes)
    poly
  }

  def createLinestring(coords: Buffer[Coordinate]): LineString = {
    var line: LineString = factory.createLineString(coords.toArray)
    line
  }

  def createPoint(coords: Buffer[Coordinate]): Point = {
    var point: Point = factory.createPoint(coords(0))
    point
  }

  def writeToElasticsearch(query: DataStream[(com.vividsolutions.jts.geom.Geometry, Long, Int, Double, Double)]): Unit = {
    println("6")
    if (writeToElasticsearch) {
      query.addSink(new ResultForElastic(elasticsearchHost, elasticsearchPort))
    }
  }

  class ResultForElastic(host: String, port: Int) extends ElasticsearchUpsertSink[(Geometry, Long, Int, Double, Double)](
    host,
    port,
    "elasticsearch",
    "test-master",
    "upit-0") {

    override def insertJson(r: (Geometry, Long, Int, Double, Double)): Map[String, AnyRef] = {
      Map(
        "driver" -> r._3.asInstanceOf[AnyRef],
        "location" -> (r._1.getCentroid().getX() + "," + r._1.getCentroid().getY()).asInstanceOf[AnyRef],
        "time" -> r._2.asInstanceOf[AnyRef],
        "minDistance" -> r._4.asInstanceOf[AnyRef],
        "maxDistance" -> r._5.asInstanceOf[AnyRef]
        )
    }
    override def indexKey(r: (Geometry, Long, Int, Double, Double)): String = {
      // index by driver id and time
      r._3.toString + "_" + r._2.toString + "_" + r._1.getCentroid().getX() + "_" + r._1.getCentroid().getY()
    }
  }

}
