package visualisation

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafx.scene.control._
import scalafx.scene.layout._
import scalafx.scene.paint.Color
import scalafx.scene.web._
import scalafx.scene.control._
import scalafx.geometry.Insets
import scalafx.beans.property.BooleanProperty
import scalafx.collections.ObservableBuffer
import mobydick.MobyDick
import com.vividsolutions.jts.geom._

import scala.collection.mutable.Buffer
import scala.collection.mutable.ArrayBuffer

object WebDemo extends JFXApp {

  var params: Constant = new Constant()
  params.trajectoryType = 1
  var coordinateString: String = ""

  var xUTM: Buffer[Double] = new ArrayBuffer[Double]
  var yUTM: Buffer[Double] = new ArrayBuffer[Double]
  var coordinates: Buffer[Coordinate] = new ArrayBuffer[Coordinate]
  val factory = new GeometryFactory()
  var windows: Array[Int] = new Array[Int](2)

  val browser = new WebView {
    hgrow = Priority.Always
    vgrow = Priority.Always
    onAlert = (e: WebEvent[_]) => println("onAlert: " + e)
    /*
    onStatusChanged = (e: WebEvent[_]) => println("onStatusChanged: " + e)
    onResized = (e: WebEvent[_]) => println("onResized: " + e)
    onVisibilityChanged = (e: WebEvent[_]) => println("onVisibilityChanged: " + e)*/
    prefWidth = 1200
    prefHeight = 1200
  }

  val engine = browser.engine
  val page = this.getClass.getResource("/demo.html").toExternalForm
  engine.load(page)

  /**
   * window radio buttons
   */

  val labelWindow = new Label {
    text = "Window type"
    padding = Insets(0, 5, 0, 0)
  }
  
  val tog1 = new ToggleGroup()

  val radioNow = new RadioButton {
    minWidth = 100
    text = "Now"
    selected = true
    toggleGroup = tog1
    onMouseClicked = handle {
      hboxWindowAll.visible = false
      hboxWindowAll.managed = false
      params.windowType = 0
    }
  }
  val radioPast = new RadioButton {
    minWidth = 100
    text = "Past"
    selected = false
    toggleGroup = tog1
    onMouseClicked = handle {
      hboxWindowAll.visible = true
      hboxWindowAll.managed = true
      params.windowType = 2
    }
  }

  val vboxRadioNowPast = new VBox {
    children = List(labelWindow, radioNow, radioPast)
    padding = Insets(20, 0, 0, 0)
    spacing = 10
  }

  /*
   * Tumbling/sliding window edit fields
   */

  val txtTumblingWindow = new TextField {
    text = "Collect time"
    minWidth = 50
  }

  val labelSecs1 = new Label {
    text = "s"
  }

  val labelSecs2 = new Label {
    text = "s"
  }

  val labelSecs3 = new Label {
    text = "s"
  }

  val txtSlidingWindowFirst = new TextField {
    text = "Collect time"
    minWidth = 50
    margin = Insets(0, 0, 5, 0)
  }

  val txtSlidingWindowSecond = new TextField {
    text = "Result time"
    minWidth = 50
  }

  val hboxTumblingWindow = new HBox {
    children = List(txtTumblingWindow, labelSecs1)
    padding = Insets(0, 0, 10, 0)
    spacing = 10
  }

  val hboxSlidingWindow1 = new HBox {
    children = List(txtSlidingWindowFirst, labelSecs2)
    padding = Insets(0, 0, 0, 0)
    spacing = 10
  }
  val hboxSlidingWindow2 = new HBox {
    children = List(txtSlidingWindowSecond, labelSecs3)
    padding = Insets(0, 0, 0, 0)
    spacing = 10
  }

  val hboxSlidingWindow = new VBox {
    children = List(hboxSlidingWindow1, hboxSlidingWindow2)
    padding = Insets(0, 0, 0, 0)
    visible = false
  }

  val vboxWindow = new VBox {
    children = List(hboxTumblingWindow, hboxSlidingWindow)
    padding = Insets(0, 0, 0, 0)
    visible = false
  }

  val stringWindows = ObservableBuffer("Tumbling window", "Sliding window")
  val windowBox = new ComboBox[String] {
    maxWidth = 150
    promptText = "Choose window type..."
    editable = false
    items = stringWindows
    margin = Insets(0, 10, 0, 0)
    onAction = handle {
      vboxWindow.visible = true
      if (this.value.value == "Sliding window") {
        hboxTumblingWindow.visible = false
		hboxTumblingWindow.managed = false
        hboxSlidingWindow.visible = true
        hboxSlidingWindow.managed = true

        params.windowType = 2
      } else {
        hboxSlidingWindow.visible = false
		hboxSlidingWindow.managed = false

        hboxTumblingWindow.managed = true
        hboxTumblingWindow.visible = true

        params.windowType = 1
      }

    }
  }

  val hboxWindowAll = new HBox {
    children = List(windowBox, vboxWindow)
    padding = Insets(20, 0, 0, 0)
    visible = false
  }

  /*
   * Keyword
   */

  val labelKeyword = new Label {
    minWidth = 100
    text = "KEYWORD"
    padding = Insets(5, 20, 0, 0)
  }
  val stringKeywords = ObservableBuffer("WITHIN", "DISJOINT", "DISTANCE", "PASSES", "TRAJECTORY LENGTH",
    "MIN DISTANCE", "MAX DISTANCE", "DIRECTION")
  val keywordsBox = new ComboBox[String] {
    maxWidth = 200
    promptText = "Choose keyword..."
    editable = false
    padding = Insets(0, 0, 0, 10)
    items = stringKeywords
    onAction = handle {
      adjustForm(this.value.value)
      params.keyword = this.value.value
      if (this.value.value.equals("MAX SPEED") || this.value.value.equals("MIN SPEED")) {
        labelValueUnit.text = "m/s"
      }
    }
  }

  val hboxKeywordSection = new HBox {
    children = List(labelKeyword, keywordsBox)
    padding = Insets(0, 0, 0, 0)
  }

  val hboxTop1 = new HBox {
    children = List(vboxRadioNowPast, hboxWindowAll)
    padding = Insets(0, 20, 10, 0)
    spacing = 10
  }

  /**
   * Operator
   */

  val labelOperator = new Label {
    minWidth = 100
    text = "Operator"
    padding = Insets(5, 20, 0, 0)
  }
  val stringOperator = ObservableBuffer("=", ">", ">=", "<", "<=")
  val operatorBox = new ComboBox[String] {
    maxWidth = 200
    promptText = "Choose operator..."
    editable = false
    padding = Insets(0, 0, 0, 20)
    items = stringOperator
    onAction = handle {
      params.operator = this.value.value
    }
  }

  val hboxOperatorSection = new HBox {
    children = List(labelOperator, operatorBox)
    padding = Insets(20, 0, 0, 0)
  }

  val labelValue = new Label {
    minWidth = 100
    text = "Value"
    padding = Insets(5, 20, 0, 0)
  }

  val txtValue = new TextField {
    text = ""
    minWidth = 150
  }

  val labelValueUnit = new Label {
    text = "m"
    padding = Insets(5, 0, 0, 5)
  }

  val hboxValueSection = new HBox {
    children = List(labelValue, txtValue, labelValueUnit)
    padding = Insets(20, 0, 0, 0)
  }

  val labelDirection = new Label {
    minWidth = 100
    text = "Direction"
    padding = Insets(5, 20, 0, 0)
  }
  val stringDirection = ObservableBuffer("North", "NorthEast", "East", "SouthEast", "South", "SouthWest", "West", "NorthWest")
  val directionBox = new ComboBox[String] {
    maxWidth = 200
    promptText = "Choose direction..."
    editable = false
    padding = Insets(0, 0, 0, 20)
    items = stringDirection
    onAction = handle {
      params.direction = this.value.value
    }
  }

  val hboxDirectionSection = new HBox {
    children = List(labelDirection, directionBox)
    padding = Insets(20, 0, 0, 0)
    visible = false
    managed = false
  }

  val labelGeometry = new Label {
    minWidth = 100
    text = "Geometry"
    padding = Insets(5, 20, 0, 0)
  }
  val geometryDirection = ObservableBuffer("Point", "Linestring", "Polygon")
  val geometryBox = new ComboBox[String] {
    maxWidth = 200
    promptText = "Choose geometry..."
    editable = false
    padding = Insets(0, 0, 0, 20)
    items = geometryDirection
    onAction = handle {
      params.geometryType = this.value.value
    }
  }

  val hboxGeometrySection = new HBox {
    children = List(labelGeometry, geometryBox)
    padding = Insets(0, 0, 0, 0)
  }

  val labelCondition = new Label {
    text = "Condition"
    padding = Insets(0, 20, 0, 0)
    margin = Insets(0, 0, 0, 10)
  }
  val conditionCheckBox = new CheckBox {
    text = "True"
    minWidth = 200
    onMouseClicked = handle {
      params.condition = this.selected.value
    }
  }

  val hboxConditionSection = new HBox {
    children = List(hboxGeometrySection)
    padding = Insets(0, 0, 0, 0)
  }

  val labelInputCoordinates = new Label {
    text = "Input Coordinates"
    padding = Insets(0, 0, 0, 0)
  }

  val labelX = new Label {
    text = "X:"
    padding = Insets(0, 5, 0, 0)
  }
  val txtXCoordinate = new TextField {
    text = ""
    minWidth = 150
  }
  val hboxXCoord = new HBox {
    children = List(labelX, txtXCoordinate)
    padding = Insets(5, 0, 5, 0)
  }

  val labelY = new Label {
    text = "Y:"
    padding = Insets(0, 5, 0, 0)
  }

  val txtYCoordinate = new TextField {
    text = ""
    minWidth = 150
  }

  val hboxYCoord = new HBox {
    children = List(labelY, txtYCoordinate)
    padding = Insets(0, 0, 0, 0)
  }

  val btnAddCoordinates = new Button("Set koordinates") {
    onMouseClicked = handle {
      val lon = txtYCoordinate.text.value
      val lat = txtXCoordinate.text.value

      xUTM.append(lat.toDouble)
      yUTM.append(lon.toDouble)

      listKoordsUTM.items.apply += InputPoint(1, lat, lon)
      val conversion = MobyDick.utmToLatLon("32 V " + lat + " " + lon)
      coordinates.append(new Coordinate(lat.toDouble, lon.toDouble))
      listKoordsLatLon.items.apply += InputPoint(1, conversion(0).toString, conversion(1).toString)
      if (!coordinateString.equals("")) {
        coordinateString += ","
      }
      coordinateString += "{lat: " + conversion(0).toString + ", lng: " + conversion(1).toString + "}"
    }
    minWidth = 200
    margin = Insets(0, 10, 0, 0)
  }

  val btnEmptyList = new Button("Empty list") {
    onMouseClicked = handle {
      listKoordsLatLon.items.apply.clear()
      listKoordsUTM.items.apply.clear()

    }
    minWidth = 200
  }

  val hboxButtonCoord = new HBox {
    children = List(btnAddCoordinates, btnEmptyList)
    padding = Insets(0, 0, 0, 0)
  }

  val vboxCoordinatesSection = new VBox {
    children = List(
      hboxGeometrySection,
      labelInputCoordinates,
      hboxXCoord,
      hboxYCoord,
      hboxButtonCoord)
    padding = Insets(10, 0, 10, 0)
    spacing = 10

  }

  case class InputPoint(id: Int, lat: String, lon: String) {
    override def toString = lat + ", " + lon
  }

  val pointsUTM = ObservableBuffer[InputPoint]()

  val pointsLatLon = ObservableBuffer[InputPoint]()

  val lblListUTM = new Label {
    text = "UTM:"
  }
  val listKoordsUTM = new ListView[InputPoint] {
    items = pointsUTM
    selectionModel().selectedItem.onChange {
      (_, _, newValue) => println("Selection Changed: " + newValue)
    }
    editable = false
    prefHeight = 200
    prefWidth = 200

  }

  val vboxUTMList = new VBox {
    children = List(lblListUTM, listKoordsUTM)
    padding = Insets(10, 0, 10, 0)
    margin = Insets(0, 10, 0, 0)
    spacing = 10
  }

  val lblListLatLon = new Label {
    text = "LatLon:"
  }
  val listKoordsLatLon = new ListView[InputPoint] {
    items = pointsLatLon
    selectionModel().selectedItem.onChange {
      (_, _, newValue) => println("Selection Changed: " + newValue)
    }
    editable = false
    prefHeight = 200
    prefWidth = 200

  }

  val vboxLatLonList = new VBox {
    children = List(lblListLatLon, listKoordsLatLon)
    padding = Insets(10, 0, 10, 0)
    spacing = 10
  }

  val hboxLists = new HBox {
    children = List(vboxUTMList, vboxLatLonList)
  }

  val tog = new ToggleGroup()

  val radioLine = new RadioButton {
    minWidth = 200
    text = "Line Trajectory"
    toggleGroup = tog
    onMouseClicked = handle {
      params.trajectoryType = 1
    }
  }
  val radioPoints = new RadioButton {
    minWidth = 200
    text = "Point-to-Point Trajectory"
    selected = true
    toggleGroup = tog
    onMouseClicked = handle {
      params.trajectoryType = 2
    }
  }

  val hboxRadio = new HBox {
    children = List(radioPoints, radioLine)
    padding = Insets(10, 0, 10, 0)
    spacing = 10
  }

  val labelChooseTrajectory = new Label {
    text = "Choose type of trajectory"
  }

  val vboxTrajectory = new VBox {
    children = List(labelChooseTrajectory, hboxRadio)

  }

  val btnRunQuery = new Button("Run query") {
    onMouseClicked = handle {
    
      if (params.windowType == 0) {
        params.windowValue = null
      } else if (params.windowType == 1) {
        windows(0) = Integer.parseInt(txtTumblingWindow.text.value.toString)
        windows(1) = -1
        params.windowValue = windows
      } else if (params.windowType == 2) {
        println(txtSlidingWindowFirst.text.toString)
        windows(0) = Integer.parseInt(txtSlidingWindowFirst.text.value.toString)
        windows(1) = Integer.parseInt(txtSlidingWindowSecond.text.value.toString)
        params.windowValue = windows
      }
      if (params.operator != null) {
        params.valueOperator = txtValue.text.value.toString.toDouble
      }
      println(params.geometryType);
      if (params.geometryType != null) {
        params.x = xUTM
        params.y = yUTM
        params.coordinates = coordinates
      }
      if ("Polygon".equals(params.geometryType)) {
        engine.executeScript(
          " var coords = [" + prepareCoordinateString() + "]; " + " drawPolygone(coords) ")
      } else if ("Linestring".equals(params.geometryType)) {
        engine.executeScript(
          " var coords = [" + prepareCoordinateString() + "]; " + " drawLinestring(coords) ")
      } else if ("Point".equals(params.geometryType)) {
        val lon = txtYCoordinate.text.value
        val lat = txtXCoordinate.text.value
     
        val conversion = MobyDick.utmToLatLon("32 V " + lat + " " + lon)
       engine.executeScript(
          " drawPoint(" + conversion(0).toString + "," + conversion(1).toString + "," + params.valueOperator + ") ")
      }

      QueryBuilder.create(params)
    }
    minWidth = 100
    margin = Insets(0, 10, 0, 0)
  }

  val btnCancelQuery = new Button("Cancel Query") {
    onMouseClicked = handle {
      //for purpose of testing
		if ("Polygon".equals(params.geometryType)) {
        engine.executeScript(
          " var coords = [" + prepareCoordinateString() + "]; " + " drawPolygone(coords) ")
      } else if ("Linestring".equals(params.geometryType)) {
        engine.executeScript(
          " var coords = [" + prepareCoordinateString() + "]; " + " drawLinestring(coords) ")
      } else if ("Point".equals(params.geometryType)) {
        val lon = txtYCoordinate.text.value
        val lat = txtXCoordinate.text.value
     
        val conversion = MobyDick.utmToLatLon("32 V " + lat + " " + lon)
        engine.executeScript(
          " drawPoint(" + conversion(0).toString + "," + conversion(1).toString + "," + txtValue.text.value.toString.toDouble + ") ")
      }
    }
    minWidth = 100
  }

  val hboxButtonExecute = new HBox {
    children = List(btnRunQuery, btnCancelQuery)
    padding = Insets(20, 0, 0, 0)
  }

  val vboxGeometrySection = new VBox {
    children = List(
      vboxCoordinatesSection,
      hboxLists)
    visible = false
    managed = false
  }

  val vboxOperatorValueSection = new VBox {
    children = List(hboxOperatorSection,
      hboxValueSection)
    visible = false
    managed = false
  }

  val vboxTopSelection = new VBox {
    children = List(hboxTop1,
      hboxKeywordSection,
      vboxOperatorValueSection,
      hboxDirectionSection,
      vboxGeometrySection,
      vboxTrajectory,
      hboxButtonExecute)
    padding = Insets(10, 0, 10, 20)
    spacing = 10

  }

  stage = new PrimaryStage {
    title = "Visualisation"
    scene = new Scene {
      fill = Color.LightGray
      root = new BorderPane {
        hgrow = Priority.Always
        vgrow = Priority.Always
        left = new BorderPane {
          center = new BorderPane {
            center = vboxTopSelection
          }
        }
        right = browser
      }
    }
    maximized = true;
  }

  def adjustForm(keyword: String): Unit = {
    if (keyword.equals("WITHIN")) {
      adjustGeometry();
    } else if (keyword.equals("DISJOINT")) {
      adjustGeometry();
    } else if (keyword.equals("DIRECTION")) {
      adjustDirection();
    } else if (keyword.equals("DISTANCE")) {
      adjustOperatorValueGeometry();
    } else if (keyword.equals("PASSES")) {
      adjustGeometry();
    } else if (keyword.equals("TRAJECTORY LENGTH")) {
      adjustOperatorValue();
      labelValueUnit.text = "m"
    } else if (keyword.equals("MIN DISTANCE")) {
      adjustGeometry();
      labelValueUnit.text = "m"
    } else if (keyword.equals("MAX DISTANCE")) {
      adjustGeometry();
      labelValueUnit.text = "m"
    }
  }

  def adjustGeometry(): Unit = {
    vboxGeometrySection.visible = true
    hboxDirectionSection.visible = false
    vboxOperatorValueSection.visible = false
    vboxGeometrySection.managed = true
    hboxDirectionSection.managed = false
    vboxOperatorValueSection.managed = false
  }

  def adjustOperatorValue(): Unit = {
    vboxGeometrySection.visible = false
    hboxDirectionSection.visible = false
    vboxOperatorValueSection.visible = true
    vboxGeometrySection.managed = false
    hboxDirectionSection.managed = false
    vboxOperatorValueSection.managed = true
  }

  def adjustDirection(): Unit = {
    vboxGeometrySection.visible = false
    hboxDirectionSection.visible = true
    vboxOperatorValueSection.visible = false
    vboxGeometrySection.managed = false
    hboxDirectionSection.managed = true
    vboxOperatorValueSection.managed = false
  }

  def adjustOperatorValueGeometry(): Unit = {
    vboxGeometrySection.visible = true
    hboxDirectionSection.visible = false
    vboxOperatorValueSection.visible = true
    vboxGeometrySection.managed = true
    hboxDirectionSection.managed = false
    vboxOperatorValueSection.managed = true
  }

  def prepareCoordinateString(): String = {
    coordinateString
  }

}


